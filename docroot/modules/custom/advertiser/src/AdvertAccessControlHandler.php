<?php

namespace Drupal\advertiser;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Advert entity.
 *
 * @see \Drupal\advertiser\Entity\Advert.
 */
class AdvertAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\advertiser\Entity\AdvertInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished advert entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published advert entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit advert entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete advert entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add advert entities');
  }


}
