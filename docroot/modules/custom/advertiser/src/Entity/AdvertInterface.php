<?php

namespace Drupal\advertiser\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Advert entities.
 *
 * @ingroup advertiser
 */
interface AdvertInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Advert name.
   *
   * @return string
   *   Name of the Advert.
   */
  public function getName();

  /**
   * Sets the Advert name.
   *
   * @param string $name
   *   The Advert name.
   *
   * @return \Drupal\advertiser\Entity\AdvertInterface
   *   The called Advert entity.
   */
  public function setName($name);

  /**
   * Gets the Advert creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Advert.
   */
  public function getCreatedTime();

  /**
   * Sets the Advert creation timestamp.
   *
   * @param int $timestamp
   *   The Advert creation timestamp.
   *
   * @return \Drupal\advertiser\Entity\AdvertInterface
   *   The called Advert entity.
   */
  public function setCreatedTime($timestamp);

}
