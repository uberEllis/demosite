<?php

/**
 * @file
 * Contains advert.page.inc.
 *
 * Page callback for Advertisement entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Advertisement templates.
 *
 * Default template: advert.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_advert(array &$variables) {
  // Fetch Advert Entity Object.
  $advert = $variables['elements']['#advert'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
