<?php

namespace Drupal\ds_demo\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\ds_demo\Entity\AdvertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdvertController.
 *
 *  Returns responses for Advertisement routes.
 */
class AdvertController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Advertisement revision.
   *
   * @param int $advert_revision
   *   The Advertisement revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($advert_revision) {
    $advert = $this->entityTypeManager()->getStorage('advert')
      ->loadRevision($advert_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('advert');

    return $view_builder->view($advert);
  }

  /**
   * Page title callback for a Advertisement revision.
   *
   * @param int $advert_revision
   *   The Advertisement revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($advert_revision) {
    $advert = $this->entityTypeManager()->getStorage('advert')
      ->loadRevision($advert_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $advert->label(),
      '%date' => $this->dateFormatter->format($advert->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Advertisement.
   *
   * @param \Drupal\ds_demo\Entity\AdvertInterface $advert
   *   A Advertisement object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AdvertInterface $advert) {
    $account = $this->currentUser();
    $advert_storage = $this->entityTypeManager()->getStorage('advert');

    $langcode = $advert->language()->getId();
    $langname = $advert->language()->getName();
    $languages = $advert->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $advert->label()]) : $this->t('Revisions for %title', ['%title' => $advert->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all advertisement revisions") || $account->hasPermission('administer advertisement entities')));
    $delete_permission = (($account->hasPermission("delete all advertisement revisions") || $account->hasPermission('administer advertisement entities')));

    $rows = [];

    $vids = $advert_storage->revisionIds($advert);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\ds_demo\AdvertInterface $revision */
      $revision = $advert_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $advert->getRevisionId()) {
          $link = $this->l($date, new Url('entity.advert.revision', [
            'advert' => $advert->id(),
            'advert_revision' => $vid,
          ]));
        }
        else {
          $link = $advert->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.advert.translation_revert', [
                'advert' => $advert->id(),
                'advert_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.advert.revision_revert', [
                'advert' => $advert->id(),
                'advert_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.advert.revision_delete', [
                'advert' => $advert->id(),
                'advert_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['advert_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
