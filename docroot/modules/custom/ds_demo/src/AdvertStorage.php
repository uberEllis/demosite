<?php

namespace Drupal\ds_demo;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\ds_demo\Entity\AdvertInterface;

/**
 * Defines the storage handler class for Advertisement entities.
 *
 * This extends the base storage class, adding required special handling for
 * Advertisement entities.
 *
 * @ingroup ds_demo
 */
class AdvertStorage extends SqlContentEntityStorage implements AdvertStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(AdvertInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {advert_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {advert_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(AdvertInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {advert_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('advert_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
