<?php

namespace Drupal\ds_demo;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Advertisement entity.
 *
 * @see \Drupal\ds_demo\Entity\Advert.
 */
class AdvertAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ds_demo\Entity\AdvertInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished advertisement entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published advertisement entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit advertisement entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete advertisement entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add advertisement entities');
  }


}
