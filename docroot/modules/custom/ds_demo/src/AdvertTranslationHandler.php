<?php

namespace Drupal\ds_demo;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for advert.
 */
class AdvertTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
