<?php

namespace Drupal\ds_demo\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Advertisement entities.
 *
 * @ingroup ds_demo
 */
class AdvertDeleteForm extends ContentEntityDeleteForm {


}
