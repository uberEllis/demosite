<?php

namespace Drupal\ds_demo;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\ds_demo\Entity\AdvertInterface;

/**
 * Defines the storage handler class for Advertisement entities.
 *
 * This extends the base storage class, adding required special handling for
 * Advertisement entities.
 *
 * @ingroup ds_demo
 */
interface AdvertStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Advertisement revision IDs for a specific Advertisement.
   *
   * @param \Drupal\ds_demo\Entity\AdvertInterface $entity
   *   The Advertisement entity.
   *
   * @return int[]
   *   Advertisement revision IDs (in ascending order).
   */
  public function revisionIds(AdvertInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Advertisement author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Advertisement revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\ds_demo\Entity\AdvertInterface $entity
   *   The Advertisement entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(AdvertInterface $entity);

  /**
   * Unsets the language for all Advertisement with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
