<?php

namespace Drupal\ds_demo\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Advertisement entities.
 */
class AdvertViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
