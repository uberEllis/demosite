<?php

namespace Drupal\ds_demo\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Advertisement entities.
 *
 * @ingroup ds_demo
 */
interface AdvertInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Advertisement name.
   *
   * @return string
   *   Name of the Advertisement.
   */
  public function getName();

  /**
   * Sets the Advertisement name.
   *
   * @param string $name
   *   The Advertisement name.
   *
   * @return \Drupal\ds_demo\Entity\AdvertInterface
   *   The called Advertisement entity.
   */
  public function setName($name);

  /**
   * Gets the Advertisement creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Advertisement.
   */
  public function getCreatedTime();

  /**
   * Sets the Advertisement creation timestamp.
   *
   * @param int $timestamp
   *   The Advertisement creation timestamp.
   *
   * @return \Drupal\ds_demo\Entity\AdvertInterface
   *   The called Advertisement entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Advertisement revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Advertisement revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\ds_demo\Entity\AdvertInterface
   *   The called Advertisement entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Advertisement revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Advertisement revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\ds_demo\Entity\AdvertInterface
   *   The called Advertisement entity.
   */
  public function setRevisionUserId($uid);

}
