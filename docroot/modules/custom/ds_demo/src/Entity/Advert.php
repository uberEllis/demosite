<?php

namespace Drupal\ds_demo\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Advertisement entity.
 *
 * @ingroup ds_demo
 *
 * @ContentEntityType(
 *   id = "advert",
 *   label = @Translation("Advertisement"),
 *   bundle_label = @Translation("Advertisement type"),
 *   handlers = {
 *     "storage" = "Drupal\ds_demo\AdvertStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ds_demo\AdvertListBuilder",
 *     "views_data" = "Drupal\ds_demo\Entity\AdvertViewsData",
 *     "translation" = "Drupal\ds_demo\AdvertTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\ds_demo\Form\AdvertForm",
 *       "add" = "Drupal\ds_demo\Form\AdvertForm",
 *       "edit" = "Drupal\ds_demo\Form\AdvertForm",
 *       "delete" = "Drupal\ds_demo\Form\AdvertDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ds_demo\AdvertHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ds_demo\AdvertAccessControlHandler",
 *   },
 *   base_table = "advert",
 *   data_table = "advert_field_data",
 *   revision_table = "advert_revision",
 *   revision_data_table = "advert_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer advertisement entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/advert/{advert}",
 *     "add-page" = "/admin/structure/advert/add",
 *     "add-form" = "/admin/structure/advert/add/{advert_type}",
 *     "edit-form" = "/admin/structure/advert/{advert}/edit",
 *     "delete-form" = "/admin/structure/advert/{advert}/delete",
 *     "version-history" = "/admin/structure/advert/{advert}/revisions",
 *     "revision" = "/admin/structure/advert/{advert}/revisions/{advert_revision}/view",
 *     "revision_revert" = "/admin/structure/advert/{advert}/revisions/{advert_revision}/revert",
 *     "revision_delete" = "/admin/structure/advert/{advert}/revisions/{advert_revision}/delete",
 *     "translation_revert" = "/admin/structure/advert/{advert}/revisions/{advert_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/advert",
 *   },
 *   bundle_entity_type = "advert_type",
 *   field_ui_base_route = "entity.advert_type.edit_form"
 * )
 */
class Advert extends EditorialContentEntityBase implements AdvertInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the advert owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Advertisement entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Advertisement is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
