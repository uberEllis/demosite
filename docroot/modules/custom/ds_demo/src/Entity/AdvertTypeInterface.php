<?php

namespace Drupal\ds_demo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Advertisement type entities.
 */
interface AdvertTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
