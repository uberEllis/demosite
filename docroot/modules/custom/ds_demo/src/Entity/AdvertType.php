<?php

namespace Drupal\ds_demo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Advertisement type entity.
 *
 * @ConfigEntityType(
 *   id = "advert_type",
 *   label = @Translation("Advertisement type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ds_demo\AdvertTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ds_demo\Form\AdvertTypeForm",
 *       "edit" = "Drupal\ds_demo\Form\AdvertTypeForm",
 *       "delete" = "Drupal\ds_demo\Form\AdvertTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ds_demo\AdvertTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "advert_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "advert",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/advert_type/{advert_type}",
 *     "add-form" = "/admin/structure/advert_type/add",
 *     "edit-form" = "/admin/structure/advert_type/{advert_type}/edit",
 *     "delete-form" = "/admin/structure/advert_type/{advert_type}/delete",
 *     "collection" = "/admin/structure/advert_type"
 *   }
 * )
 */
class AdvertType extends ConfigEntityBundleBase implements AdvertTypeInterface {

  /**
   * The Advertisement type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Advertisement type label.
   *
   * @var string
   */
  protected $label;

}
